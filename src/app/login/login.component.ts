import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isConnected : boolean = false;
  constructor(private authServe : AuthService) { }

  ngOnInit(): void {
    // this.isConnected = this.authServe.isConnected;
    this.authServe.isConnected$.subscribe({
      next : (connectionState : boolean) => {
        this.isConnected = connectionState;
        console.log("Coucou Login");
      },
      error : () => {},
      complete : () => {}
    })
  }

  login(){
    // this.isConnected = this.authServe.login()
    this.authServe.login();
  }

  logout(){
    // this.isConnected = this.authServe.logout();
    this.authServe.logout();
  }

}
