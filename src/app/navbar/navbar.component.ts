import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isConnected : boolean = false;
  constructor(private authServe : AuthService) { }

  ngOnInit(): void {
    // this.isConnected = this.authServe.isConnected;
    this.authServe.isConnected$.subscribe({
      next : (connectionState : boolean) => {
        this.isConnected = connectionState;
        console.log("Coucou NavBar");
      },
      error : () => {},
      complete : () => {}
    })
  }

}
