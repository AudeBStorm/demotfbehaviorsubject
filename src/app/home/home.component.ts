import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  isConnected : boolean = false;

  constructor(private authServe : AuthService) { }

  ngOnInit(): void {
    // this.isConnected = this.authServe.isConnected;
    this.authServe.isConnected$.subscribe({
      next : (connectionState : boolean) => {
        this.isConnected = connectionState;
        console.log("Coucou Home");
        
      },
      error : () => {},
      complete : () => {}
    })
  }

}
