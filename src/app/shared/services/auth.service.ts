import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  //isConnected : boolean = false;
  //isConnected$ : Subject<boolean> = new Subject();
  isConnected$ : BehaviorSubject<boolean> = new BehaviorSubject(false);


  constructor() { }

  login(){
    //return this.isConnected = true;
    this.isConnected$.next(true)
  }

  logout(){
    //return this.isConnected = false;
    this.isConnected$.next(false);
  }
}
